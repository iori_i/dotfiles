HOST=`hostname`
USER=`whoami`

if [[ $HOST =~ now[0-9]+ ]]; then
  if [ -e "/$HOST/$USER" ]; then
    export LOCALHOME="/$HOST/$USER/home"

    # Create workspace
    if ! [ -e $LOCALHOME ]; then
      mkdir -p $LOCALHOME
      cp /poisson3/home3/i.ikeda/dotfiles/now.zshrc "$LOCALHOME/.zshrc"

      # install pyenv + virtualenv
      mkdir -p "/$HOST/$USER/bin"
      cd "/$HOST/$USER/bin"
      git clone https://github.com/pyenv/pyenv.git
      cd pyenv/plugins
      git clone https://github.com/pyenv/pyenv-virtualenv.git

      export PYENV_ROOT="/$HOST/$USER/bin/pyenv"
      export PATH="$PYENV_ROOT/bin:/$HOST/$USER/bin:$PATH"
      eval "$(pyenv init -)"
      eval "$(pyenv virtualenv-init -)"
      ANACONDA=`pyenv install -l | grep -e "anaconda3-[0-9]\+\.[0-9]\+\.[0-9]\+" | tail -n 1`
      pyenv install $ANACONDA
    fi

    if ! [ -e "$LOCALHOME/docker" ]; then
      cp -r /poisson3/home3/i.ikeda/docker "$LOCALHOME/docker"
    fi

    if ! [ -e "$LOCALHOME/data" ]; then
      mkdir "$LOCALHOME/data"
      cp /poisson3/home3/i.ikeda/data/*.sh "$LOCALHOME/data"
    fi

    if [ -e "$LOCALHOME/.zshrc" ]; then
      source "$LOCALHOME/.zshrc"
    else
      echo "$HOST's .zshrc not found!"
    fi

    cd "$LOCALHOME"
  else
    echo "/$HOST/$USER directory is not found."
  fi
fi

export LC_TIME=C
export LANG=ja_JP.UTF-8

zstyle ':completion::complete:*' use-cache true
zstyle ':completion:*:default' menu select=1
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

autoload colors
zstyle ':completion:*' list-colors "${LS_COLORS}"

# alias
alias emacs="vim"
alias vim="vim -p"
alias gs="git status"
alias gcho="git checkout"
alias gl="git log --stat -1"
alias gls="git log --stat"
alias glp="git log -p"
alias glo="git log --oneline"
alias glog="git log --oneline --graph"
alias gb="git branch"
alias gdc="git diff --cached"
alias md="mkdir -p"
alias ll="ls -lFh"
alias la="ls -lFAh"
alias dp="docker ps"
alias da="docker attach"
alias cdhome="cd $LOCALHOME"
alias cdp="cd /poisson3/home3/$USER"

alias anashell="anakeras -v work:/work -v tmp:/work/tmp"

#restartと入力したらshellを再起動する
function restart(){
  exec $SHELL -l
}

function tm() {
    if [ -n "${1}" ]; then
        tmux attach-session-t ${1} || \
        tmux new-session -s ${1}
    else
        tmux attach-session || \
        tmux new-session
    fi
}

# コマンドを入力しない場合git statusとlsを表示する
function do_enter() {
  if [ -n "$BUFFER" ]; then
    zle accept-line
    return 0
  fi
  echo
  ls -lF
  if [ "$(git rev-parse --is-inside-work-tree 2> /dev/null)" = 'true' ]; then
    echo
    echo -e "\e[0;33m--- git status ---\e[0m"
    git status -sb
    echo
    echo -e "\e[0;33m--- git log ---\e[0m"
    echo `git log --oneline -1`
  fi
  zle reset-prompt
  return 0
}
zle -N do_enter
bindkey '^m' do_enter

# ctrl+zでバックグラウンドフォアグランドを切り替える
fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z

# pip local
export PATH="/home/i.ikeda/.local/bin:$PATH"

# plugins
source /home/i.ikeda/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
if [ -e ~/.zsh/completions ]; then
  fpath=(~/.zsh/completions $fpath)
fi

autoload -Uz compinit && compinit -i
