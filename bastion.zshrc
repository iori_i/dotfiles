export LC_TIME=C
export LANG=ja_JP.UTF-8

zstyle ':completion::complete:*' use-cache true
zstyle ':completion:*:default' menu select=1
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

autoload colors
zstyle ':completion:*' list-colors "${LS_COLORS}"

# alias
alias emacs="vim"
alias vim="vim -p"
alias md="mkdir -p"
alias ll="ls -lF"
alias la="ls -lFA"

#restartと入力したらshellを再起動する
function restart(){
  exec $SHELL -l
}

function tm() {
    if [ -n "${1}" ]; then
        tmux attach-session-t ${1} || \
        tmux new-session -s ${1}
    else
        tmux attach-session || \
        tmux new-session
    fi
}

autoload -Uz compinit && compinit -i

export PATH=${HOME}/local/bin:$PATH
export LD_LIBRARY_PATH=${HOME}/local/lib:$LD_LIBRARY_PATH
