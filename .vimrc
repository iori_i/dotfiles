"if the Vim version is lower than 7.4, use dein 1.5.
"git checkout a80906fbc2

if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/home/i.ikeda/.vim/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/home/i.ikeda/.vim/dein')
  call dein#begin('/home/i.ikeda/.vim/dein')

  " Let dein manage dein
  " Required:
  call dein#add('/home/i.ikeda/.vim/dein/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here like this:
  "call dein#add('Shougo/neosnippet.vim')
  "call dein#add('Shougo/neosnippet-snippets')

  call dein#add('Shougo/neocomplete.vim')             " Completion
  call dein#add('nvie/vim-flake8')                    " Python
  "call dein#add('fatih/vim-go')                      " Go
  call dein#add('nathanaelkane/vim-indent-guides')    " visually displaying indent levels
  call dein#add('scrooloose/nerdtree')
  call dein#add('Shougo/unite.vim')
  call dein#add('prabirshrestha/vim-lsp')             " lsp(language server protocol) client
  call dein#add('prabirshrestha/async.vim')           " for vim-lsp
  call dein#add('prabirshrestha/asyncomplete.vim')
  call dein#add('prabirshrestha/asyncomplete-lsp.vim')
  call dein#add('ujihisa/neco-look')                  " English words completion

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

let g:lsp_log_verbose = 1  " debug log
"let g:lsp_log_file = expand('~/.cache/tmp/vim-lsp.log')
"let g:asyncomplete_log_file = expand('~/.cache/tmp/asyncomplete.log')

" lsp settings
augroup MyLsp
  autocmd!
  " pip install python-language-server
  if executable('pyls')
    " for Python
    " - disable pycodestyle
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'pyls',
          \ 'cmd': { server_info -> ['pyls'] },
          \ 'whitelist': ['python'],
          \ 'workspace_config': {'pyls': {'plugins': {
          \   'pycodestyle': {'enabled': v:false},
          \   'jedi_definition': {'follow_imports': v:true, 'follow_builtin_imports': v:true},}}}
          \})
    autocmd FileType python call s:configure_lsp()
  endif
  " go get -u golang.org/x/tools/cmd/gopls
  if executable('gopls')
    " for Golang
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'gopls',
          \ 'cmd': { server_info -> ['gopls', '-mode', 'stdio'] },
          \ 'whitelist': ['go'],
          \})
    autocmd FileType go call s:configure_lsp()
  endif
augroup END
function! s:configure_lsp() abort
  setlocal omnifunc=lsp#complete   " enable omni completion
  " keymap for LSP
  nnoremap <buffer> <C-]> :<C-u>LspDefinition<CR>
  nnoremap <buffer> gd :<C-u>LspDefinition<CR>
  nnoremap <buffer> gD :<C-u>LspReferences<CR>
  nnoremap <buffer> gs :<C-u>LspDocumentSymbol<CR>
  nnoremap <buffer> gS :<C-u>LspWorkspaceSymbol<CR>
  nnoremap <buffer> gQ :<C-u>LspDocumentFormat<CR>
  vnoremap <buffer> gQ :LspDocumentRangeFormat<CR>
  nnoremap <buffer> K :<C-u>LspHover<CR>
  nnoremap <buffer> <F1> :<C-u>LspImplementation<CR>
  nnoremap <buffer> <F2> :<C-u>LspRename<CR>
endfunction
let g:lsp_diagnostics_enabled = 0  " disable warning and error

" plugin setting
let g:neocomplete#enable_at_startup = 1
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1
inoremap <expr><CR>  neocomplete#smart_close_popup() . "\<CR>"
inoremap <expr><Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr><S-Tab> pumvisible() ? "\<C-n>" : "\<S-Tab>"
call neocomplete#is_text_mode()
let g:neocomplete#text_mode_filetypes = { "_" : 1 }

" vim-go
"let g:go_bin_path = $LOCALHOME.'/go/bin'
"let g:go_fmt_command = 'goimports'
"let g:go_def_mapping_enabled = 0
"let g:go_dco_keywordprg_enabled = 0

" move by cursor
set whichwrap=b,s,h,l,<,>,[,],~

" delete by backspace
set backspace=indent,eol,start

" file setting
set encoding=utf-8
set fileencoding=utf-8
set noswapfile
set nobackup

" clipboard
set clipboard+=unnamed
set clipboard+=unnamed
set paste

" line highlight
set cursorline
hi clear cursorline

" search
set incsearch
set hlsearch
set wildmenu

" syntax highlight
colorscheme hybrid
set background=dark

" indent
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set smartindent
set cindent
set smarttab

" completion
inoremap {<Enter> {}}<Left><CR><ESC><S-o>
inoremap {<Enter> []}<Left><CR><ESC><S-o>
inoremap {<Enter> ()}<Left><CR><ESC><S-o>

" delete last space
autocmd BufWritePre * :%s/\s\+$//ge

" status line
let g:currentmode={
      \ 'n'  : 'Normal ',
      \ 'no' : 'N·Operator Pending ',
      \ 'v'  : 'Visual ',
      \ 'V'  : 'V-Line ',
      \ 'x22' : 'V-Block ',
      \ 's'  : 'Select ',
      \ 'S'  : 'S·Line ',
      \ 'x19' : 'S·Block ',
      \ 'i'  : 'Insert ',
      \ 'R'  : 'Replace ',
      \ 'Rv' : 'V·Replace ',
      \ 'c'  : 'Command ',
      \ 'cv' : 'Vim Ex ',
      \ 'ce' : 'Ex ',
      \ 'r'  : 'Prompt ',
      \ 'rm' : 'More ',
      \ 'r?' : 'Confirm ',
      \ '!'  : 'Shell ',
      \ 't'  : 'Terminal '
      \}

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

function! ChangeStatuslineColor()
  if (mode() =~# '\v(n|no)')
    exe 'hi! StatusLine term=None cterm=bold ctermfg=DarkGray ctermbg=White'
  elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
    exe 'hi! StatusLine term=None cterm=bold ctermfg=White ctermbg=DarkGreen'
  elseif (mode() ==# 'i')
    exe 'hi! StatusLine term=None cterm=bold ctermfg=White ctermbg=DarkCyan'
  else
    exe 'hi! StatusLine term=None cterm=bold ctermfg=Red ctermbg=White'
  endif
  return ''
endfunction

set laststatus=2
set statusline=
set statusline+=%{ChangeStatuslineColor()}
set statusline+=%0*\ %{toupper(g:currentmode[mode()])}
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ [%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
"highlight StatusLine term=NONE cterm=NONE ctermfg=black ctermbg=white

" insert mode
inoremap <silent> jj <Esc>
vnoremap <silent> <C-p> "0p<CR>

inoremap Y y$

" other
set title
set mouse=a
set virtualedit=all
set history=5000
set list
set listchars=tab:»-,trail:-,extends:»,precedes:«,nbsp:%,eol:↲
set number
set ruler
set showcmd
set showmatch
set nowrap
set scrolloff=5
set nostartofline
set matchtime=3
set textwidth=0
set laststatus=2
set pumheight=10
set nohlsearch
set paste

if !has('gui_running')
  set timeout timeoutlen=1000 ttimeoutlen=50
endif

set t_kD=^?

autocmd FileType python setl autoindent
autocmd FileType python setl smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd FileType python setl tabstop=4 expandtab shiftwidth=2 softtabstop=2
autocmd FileType python setl completeopt-=preview

autocmd FileType go setl tabstop=4 noexpandtab shiftwidth=4

augroup MyXML
  autocmd!
  autocmd Filetype xml inoremap <buffer> </ </<C-x><C-o>
  autocmd Filetype html inoremap <buffer> </ </<C-x><C-o>
  autocmd Filetype xsl inoremap <buffer> </ </<C-x><C-o>
augroup END

" スペースキーにLeaderを割り当て
let mapleader = "\<Space>"

" NERDTree用バインド
nnoremap <silent><C-e> :NERDTreeToggle<CR>

" Ctrl+pで貼り付けする際にレジスタにコピーしない
vnoremap <silent> <C-p> "0p<CR>

" Yでカーソルから行末までヤンク
nnoremap Y y$

" ある文字で囲む
nnoremap <Leader>" ciw""<Esc>P
nnoremap <Leader>' ciw''<Esc>P
nnoremap <Leader>` ciw``<Esc>P
nnoremap <Leader>( ciw()<Esc>P
nnoremap <Leader>{ ciw{}<Esc>P
nnoremap <Leader>[ ciw[]<Esc>P

" quickhl
nmap <Leader>m <Plug>(quickhl-manual-this)
xmap <Leader>m <Plug>(quickhl-manual-this)
nmap <Leader>M <Plug>(quickhl-manual-reset)
xmap <Leader>M <Plug>(quickhl-manual-reset)

" その他
nnoremap <Leader>w :w<CR>
nnoremap <Leader>x :x<CR>
nnoremap <Leader>q :q<CR>

" beep音
set visualbell t_vb=

" 入力モード中に素早くJJと入力した場合はESCとみなす
inoremap <silent> jj <ESC>

