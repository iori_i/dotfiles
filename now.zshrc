HOST=`hostname`
USER=`whoami`
export PYENV_ROOT="/$HOST/$USER/bin/pyenv"
export PATH="$PYENV_ROOT/bin:/$HOST/$USER/bin:.:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

export HISTFILE=${LOCALHOME}/.zsh_history
export HISTSIZE=1000
export SAVEHIST=100000

